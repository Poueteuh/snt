## Le numérique est-il un domaine réservé aux hommes ?

> Si on retient volontier les noms de grands hommes ayant révolutionné l'informatique, très peu d'entre nous connaissent le nom des femmes qui en ont fait tout autant !

![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/We_Can_Do_It%21.jpg/185px-We_Can_Do_It%21.jpg)
_Affiche créée pour Westinghouse en 1942 par J. Howard Miller. (Wikipédia)_

Selon une [étude de l'INSEE](https://www.insee.fr/fr/statistiques/4126588?sommaire=4238635), entre 2009 et 2017, publiée en 2019, les hommes occupent  77 % des emplois du numérique. Autrement dit, les __femmes__ n'occupent que __23% des emplois du numérique__. 

Une [autre étude](https://publications.europa.eu/en/publication-detail/-/publication/84bd6dea-2351-11e8-ac73-01aa75ed71a1/language-fr) de 2015 montre que __57 % de l’ensemble des diplômés étaient des femmes__, mais seulement __25 % ont obtenu un diplôme dans les filières du numérique__.

De manière plus proche, à la rentrée 2019, __2,6% de filles de secondes__ ont choisi la spécialité _Numérique et Science Informatique (NSI)_ en première, contre 15,2% des garçons, selon les [statistiques d'inscription](https://www.education.gouv.fr/choix-de-trois-specialites-en-premiere-generale-la-rentree-2019-15-combinaisons-pour-80-des-eleves-3245).

Une multitude de questions émergent : "__Pourquoi cette inégalité femme/homme dans le numérique ?__ Cette inégalité a-t-elle toujours existé ou est-elle entretenue ? Quelles sont les causes de cette inégalité ? __L'informatique moderne aurait-il existé sans les femmes ?__

L'activité a pour but de trouver des éléments de réponses à toutes ces questions.

### Consignes

Vous disposez des ressources suivantes :

- __Ressource 1__ : Vidéo [L'histoire des femmes qui ont fait l'Internet](https://www.youtube.com/watch?v=STKZFXmrIiA&ab_channel=Telerama), publiée le 28/03/2019 sur la chaine Youtube Telerama
- __Ressource 2__ : Article [Ces femmes qui ont révolutionné l’informatique moderne](https://usbeketrica.com/fr/article/elles-ont-revolutionne-l-informatique-moderne), publié le 16/01/2018 sur le site usbeketrica
- __Ressource 3__ : Extrait du livre _[Les oubliées du numérique](./assets/les_oubliees_du_numerique.pdf)_ d'Isabelle Collet, paru en 2019 aux éditions Le Passeur, sur la place des femmes dans le numérique en Malaisie (ISBN : 9782368907054)

En **une séance**, Vous devez sur **un document word, libre office, open office**, répondre de manière détaillée aux points suivants :

1. Citez au moins 8 femmes et dites en quoi elles ont contribué à l'informatique (Ressource 1 et 2).
2. Décrivez quelle était la place des femmes dans l'informatique des années 40-50 (Ressource 1).
3. Comment était considéré le travail de programmeuse à cette époque ? (Ressource 1)
4. Selon Claire L. Evans, quelles sont les causes de l'éviction des femmes du numérique ? (Ressource 1)
5. Expliquez pourquoi les femmes sont plus présentes dans le numérique en Malaisie qu'en France. (Ressource 3)



