# Bienvenue en SNT !

## Tout d'abord : Je vous souhaite une excellente année de seconde.

![giphy](assets/giphy.gif)


## SNT, qu'est ce que c'est ?

###  Sciences Numériques et Technologie

## Pourquoi ?

- Découvrir et s'approprier les principaux concepts du numérique
- Comprendre les nouvelles technologies, les enjeux, ainsi que l'impact sur notre environnement
-  Réaliser que nos actions en ligne ont des conséquences sur notre entourage
- Améliorer sa culture générale et informatique

## Comment ?

- *__Reflechir__* sur nos usages ainsi que sur notre relation à la technologie
-  S'appuyer sur nos <b>connaissances</b> provenants d'autres matières
- Travailler ensemble, en équipe, <b> coopérer </b>
- Apprendre à faire des recherches, à <b> croiser les informations </b> 
-  Developper une <b> autonomie </b>       

## Cette année

![Thèmes](assets/Thèmes.PNG)


## On va donc parler du oueb et d'interneteuh, super !

![internet-simpsons](assets/internet-simpsons.gif)

## Oui mais aussi, de la programmation python !

## Les supports 

- Version numérique sur un dépôt.

- Version numérique sur le cahier de texte dans Pronote 
- Version papier quand cela sera nécessaire     

  >  Cela veut dire que tout le monde peut accéder aux cours de n'importe où et n'importe quand, quelle joie !

## Et le matériel personnel ?

1. Un cahier / classeur, tout dépend de vos préférences.
2. Clé USB pour enregistrer le travail et pouvoir poursuivre à la maison si besoin (de très rares fois)
3. Des écouteurs ou un casque filaire pour les vidéos et extraits sonores quand nous serons en demi-groupe
4. Téléphone avec écouteurs si possible quand nous serons en classe entière
5. De l'attention ! 

## Aujourd'hui

1. Faire une fiche de connaissances
2. Voir les outils que l'on va utiliser
3. Conditions d'évaluation
        

## La fiche de connaissances

Dites moi qui vous êtes ! 
1. Nom
2. Prénom
3. Professions des parents
4. Métier souhaité plus tard ou domaine qui vous plait
5. Matériel informatique à la maison

## Les outils

1. Thonny pour programmer en python
2. Gimp pour retravailler des images
3. Notepad ++ pour écrire dans d'autres langages de programmation
4. LibreOffice pour taper ses notes ou suivre le cours
5. Firefox pour se promener sur le web

## Evaluation

- Ne pas avoir peur de l'évaluation, si on travaille sérieusement, aucun souci en vu !

- Vous avez le droit à l'erreur. 

- Formes : QCM, TP, Exposés

## En classe

- Le téléphone sur la table, je peux le regarder pour avoir l'heure si besoin, mais je ne balance pas de snapchat ou de tiktok aux copains copines pendant le cours. Sinon, gare au prof !
- Quand le professeur le demande, alors je peux utiliser mon téléphone dans le cadre du cours ou de l'activité.



## Pour internet ?

Nous utiliserons le fabuleux pouvoir d'internet pendant les TP, pendant les activités et autres exercices.

Mais, pendant le cours, on écoute le professeur, qui a beaucoup de choses à vous apprendre !

Et si vous ne le faites pas....

![no-more](assets/no-more.gif)

## Et maintenant ?

Et bien on commence les [activités !](introduction/README.md)
