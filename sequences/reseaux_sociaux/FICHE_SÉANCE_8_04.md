> Séance sur les Réseaux Sociaux, avec des débats permettant de travailler les exposés donnés aux élèves.

### Débats :

- [Les réseaux sociaux sont-ils une drogue / alcool ?](RS_alcool.mp3)
- [Cyberharcèlement, qu'est ce que c'est ?](Cyber-harcèlement.mp4)
- [Les bonnes habitudes à prendre sur les réseaux sociaux](bonnes_habitudes_facebook.mp4)
- Jeu ?