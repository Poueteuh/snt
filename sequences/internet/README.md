# L'internet

## Présentation

Grâce à sa souplesse et à son universalité, Internet est devenu le moyen de communication principal entre les hommes et avec les machines.

Ce chapitre a pour objectif de répondre aux questions suivantes : 

- Qu'est-ce qu'__Internet__ ?
- Comment __fonctionne__-t-il ?

## Progression





```mermaid
flowchart LR;
    A[1. Définition]-->B[2. Adresse IP];
    B-->C[3. Adresses symboliques et DNS];
    C-->D[4. Protocole TCP];
    C-->E[5. Réseaux pair-à-pair];
    
    click A ".https://gitlab.com/FlorianMathieu/snt/-/tree/main/sequences/internet/definition" "Lien vers 1. Définition"
    click B "./ip" "Lien vers 2. Adresse IP"
    click C "https://gitlab.com/FlorianMathieu/snt/-/tree/main/sequences/internet/dns" "Lien vers 3. Adresses symboliques et DNS"
    click D "https://gitlab.com/FlorianMathieu/snt/-/tree/main/sequences/internet/tcp" "Lien vers 4. Protocole TCP"
    click E "https://gitlab.com/FlorianMathieu/snt/-/tree/main/sequences/internet/p2p" "Lien vers 5. Réseaux pair-à-pair"
```

## Attendus du Programme

| Contenus                                                     | Capacités attendues                                          |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Protocole TCP/IP : paquets,<br />routage des paquets | Distinguer le rôle des protocoles IP et TCP.<br />Caractériser les principes du routage et ses limites.<br />Distinguer la fiabilité de transmission et l’absence de garantie temporelle. |
| Adresses symboliques et serveurs DNS | Sur des exemples réels, retrouver une adresse IP à partir d’une adresse symbolique et inversement. |
| Réseaux pair-à-pair | Décrire l’intérêt des réseaux pair-à-pair ainsi que les usages illicites qu’on peut en faire. |
| Indépendance d’internet par rapport au réseau physique | Caractériser quelques types de réseaux physiques : obsolètes ou actuels, rapides ou lents, filaires ou non.<br />Caractériser l’ordre de grandeur du trafic de données sur internet et son évolution. |