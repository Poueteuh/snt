# Réseaux Peer to Peer

> Evolution majeur du web 2.0, les réseaux pair à pair ou P2P ont offert une certaine notoriété à de nombreuses applications de téléchargements illégaux.
>
> Oubliés à l'aube des années 2010, ils sont pourtant à l'origine même de concepts en plein essor, les cryptomonnaies et la blockchain. Mais qu'est ce qu'un réseau peer to peer ?



"Je vais télécharger ce film de vacances sur Emule" "Mamie vient de me faire parvenir un super cd de chants libres de droits obtenu via BitTorrent". En 1997, un nouveau modèle informatique émerge et avec lui, ce jargon si étrange qui apparaît dans tous les conversations.

## Naissance

Utilisé principalement à des fins [illégales](https://fr.wikipedia.org/wiki/Partage_de_fichiers_en_pair-%C3%A0-pair), le peer to peer prend sa source dans des logiciels tels que Napster, eMule ou encore Bitorrent. Avec lui, de nouveaux protocoles apparaissent :

-  eDonkey, Torrent
-  BOINC
- VNC
- Skype 

> Selon vous, à quoi peuvent servir ces protocoles ?



- Le transfert de fichiers

- Le calcul distribué

- La prise en main à distance 

- Les communications

## Echapper au contrôle 

Le modèle classique sur lequel repose Internet s'appelle *__Client - Serveur__* : lorsque vous souhaitez télécharger des données, vous connecter à un service, dans la majeure partie des cas, votre machine va effectuer une requete auprès d'un serveur, qui lui répondra avant d'ouvrir un canal de transmission, comme vu précédemment avec le protocole [TCP/IP](../tcp/README.md).

Ces serveurs sont plus performants, plus rapides, et plus chers que nos machines personnelles.

Ce modèle se base donc sur la centralisation : toutes les machines qui necessitent un même service ou fichier, vont effectuer les requetes auprès du même serveur dédié à ce service.



![client_serveur.png](assets/client_serveur.png)



> Selon vous, quel est alors l'intérêt de la centralisation des données, d'un point de vue :
>
> - Légal ?
> - Matériel ?

- Plus grande facilité de contrôle des données et de l'usage qui en est fait
- Un seul serveur peut fournir des services à plusieurs milliers de personnes

------------------

Avec l'esprit des années 90 et le retour aux sources d'Internet, la décentralisation prend une place de plus en plus importante : il faut dire que c'est le principal intérêt du réseau mondiale : chaque machine peut héberger du contenu et le partager avec n'importe qui sur la planète.

Profitant d'un flou juridique - à l'époque, les leglislateurs ne s'étaient alors pas encore penchés sur notre principal outil de communication - les internautes du monde entier se mettent alors à échanger, communiquer, partager toute sorte de fichier en dehors de toute surveillance des géants du web.

![p2p.gif](assets/p2p.gif)

> 

## Aujourd'hui



Que celà soit dans les jeux (World of Warcraft, [Nintendo 3DS](https://www.switch-actu.fr/site/tribunes/nintendo-et-le-online-une-idylle-depassionnee-tribune/)), les systèmes d'exploitation [libres](https://doc.ubuntu-fr.org/p2p#reseau_bittorrent) comme [propriétaires](https://www.numerama.com/magazine/32489-windows-10-du-p2p-pour-les-mises-a-jour-et-applications.html) ou encore les [cryptomonnaies](https://www.lemonde.fr/big-browser/article/2018/09/01/la-premiere-blockchain-de-l-histoire-date-de-1995-et-elle-est-imprimee-sur-papier_5349082_4832693.html) l'usage des réseaux p2p perdurent, à travers des applications et usages, pour la plupart quotidiennement, sans que le grand public soit forcément au courant de son utilisation.

> Connaissez vous une technologie sans fil qui est également un réseau pair à pair ? Indice : certain-e-s d'entre vous l'utilisent tous les jours sur leur téléphone.
>
> Après avoir lu cet [article](https://www.numerama.com/magazine/18419-un-fai-canadien-restreint-par-erreur-world-of-warcraft-en-voulant-brider-le-p2p.html), que pouvez vous énoncer comme problèmes liés à l'usage des réseaux pair à pair ?

- Le bluetooth
- Impossible de différencier le bon du mauvais p2p 
- Même en usant d'échanges entre pairs, votre fai peut toujours vous coupez l'accès à un service ou vous réduire fortement l'accès 

Activité : cherchez ce qu'est la  [Neutralité du net](https://www.arcep.fr/nos-sujets/la-neutralite-du-net.html) et expliquez, avec vos mots, en quoi elle nous concerne tous et toutes.

Sources : 

- [Wikipedia](https://fr.wikipedia.org/wiki/Pair-%C3%A0-pair)
- [Histoire-internet](https://histoire-internet.vincaria.net/2020/04/1997-peer-to-peer-p2p.html)
- [Numérama](https://www.numerama.com)
- [ARCEP](https://www.arcep.fr)

--------------------------



## Synthèse



- Les réseaux ............................. existent depuis les années 90 et permettent les services suivants:
  - ..............................
  - ..............................
  - ..............................
  - ..............................
- Le **modèle** *Peer to Peer* s'oppose au modèle .............................. qui est le plus répandu sur Internet.
- Parmi toutes les technologies utilisant le P2P, on peut citer le ..............................
