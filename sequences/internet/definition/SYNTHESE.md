## Synthèse

- On appelle **réseau** (***network***)  un ensemble d'**équipements** reliés entre eux pour échanger des informations.
- On appelle ces appareils  **hôtes** (ou host), c'est à dire toutes les machines échangeant des données sur le réseau.

- Parmi ces hôtes, il y a :
  - Les **clients** : par extension, ce sont les machines qui utilisent un logiciel, appelé client, qui envoie des requêtes l'autre type d'hôte, c'est à dire les serveurs.
  - Les **serveurs** : par extension, ce sont les machines qui utilisent un logiciel, appelé serveur, qui attend les demandes des clients et y répond, s'il le peut.

**Est-ce-que Internet et le Web c'est la même chose ?**

- **Internet** est un réseau d'ordinateurs qui communiquent entre eux à l'aide de protocoles. C'est comparable au réseaux routiers, où le code de la route en est un *protocole*,
- **Le Web** est l'une des applications qui utilise **internet**. Il y en a d'autres : courrier électronique, transfert de fichiers, voip pour la téléphonie etc...C'est comparable aux voitures sur le réseau routier, où chacune emprunte la route, en respectant le code

## Définition d'Internet 

**Internet** représente l'ensemble des réseaux qui sont interconnectés entre eux à travers le monde, grâce à différents **protocoles**.

Il fournit des **services** comme le  **Web**, le FTP, la messagerie et les groupes de discussion.

## Définition des Protocoles

Un protocole est un ensemble de règles de communication entre deux équipements.