## Un protocole pour communiquer

### La communication

Nous sommes près de 2 milliards d'internautes, comment est-il possible de faire communiquer autant d'ordinateurs ?

C'est déjà pas simple de se faire entendre dans un groupe de 10 personnes, quasi impossible dans un groupe de 100 personnes, alors imaginez 2 milliards d'individus !

C'est le défi relevé par internet : Pouvoir communiquer tous ensemble, en même temps, et ce, quand nous le souhaitons.

Pour résoudre ce défi, il a fallu créer un modèle de communication permettant aux machines de parler entre elles.

### Les bases de la communication

Deux personnes qui communiquent, utilisent des __règles de communications__ qui sont communes à tout le monde comme le montre ce schéma.

![Communication entre 2 humains](assets/communication.jpg)

([source de l'image](http://hazmat.free.fr/1-internet/tcp-ip.html))

On peut même rajouter, que la réponse est un 'feedback', qui assure que le message a bien été réceptionné.
Pour une __communication téléphonique__, c'est à peu près la même chose, sauf que le son doit être transformé en signaux électriques pour le transport.

Pour une __lettre envoyé par la poste__, c'est essentiellement le mode de transmission qui diffère :

- Un support de transmission : La lettre
- Un contenant : l’enveloppe
- Un intermédiaire : La poste

Et comment cela se passe t-il ? 

- Pour envoyer un message :

​												 ![envoi](assets/envoi-colis.jpg)

- Et pour recevoir un message :

<img src="assets/paquet-poste.jpg" alt="reception" style="zoom: 67%;" />



__Ces règles de communications s'appellent des protocoles__.

### Le principe de communication entre ordinateurs

Les règles de communications (Protocoles) entre ordinateurs doivent se soumettre à certaines contraintes  :

- Indépendance vis à vis du matériel (constructeurs, logiciels, etc...)

- Identification unique de l'émetteur et du récepteur.

- Une gestion des flux de données (tailles, pertes..).

- Transport des données sous formes de 'bit' ( des 0 et des 1)

- et d'autres....

Cela pour que les réseaux soient __compatibles entre eux__.

__Ces contraintes sont en partie réalisées par le protocole TCP/IP__

Regardons ensemble ce qu'est ce protocole : 

[video sur le protocole tcp](https://www.youtube.com/watch?v=3MOe22OSQ1A)

Les différentes couches ont des rôles bien spécifiques :

- La couche __Application__ : Son rôle est principalement de choisir le mode de transmission ( ce sont des protocoles comme `http`, `https`, `ftp`, ...)
- La couche __Transport__ : Une fois choisi le mode de transport, cette couche est chargée de le mettre en œuvre. Pour simplifier, deux protocoles sont disponibles : __UDP__ (User Datagram Protocol) et __TCP__ (Transmission Control Protocol).
	- __TCP__ est un protocole fiable, qui permet l'acheminement sans erreur de données issues d'une machine à une autre machine . Son rôle est de fragmenter le message à transmettre de manière à pouvoir le faire passer sur la couche internet. A l'inverse, sur la machine destination, TCP replace dans l'ordre les fragments transmis sur la couche internet pour reconstruire le message initial.
	- __UDP__ est en revanche un protocole plus simple que TCP. Son utilisation présuppose que l'on n'a pas besoin de la conservation de l'ordre de remise des paquets. il n'y a pas vérification de l'arrivée de tous les paquets, ( très utile pour la transmission de vidéos...)
- La couche __Internet__ : Cette couche réalise l'interconnexion des réseaux et ce à l'aide du protocole __IP__ (Internet Protocol ). Elle permet d'acheminer les données au bon destinataire dans le réseau, en laissant aux couches supérieures le soin de les réordonner (TCP) et de les interpréter (Application)
- La couche __Physique__ : Cette couche concrétise le transport de données ( câbles, wi-fi )

Même si en réalité c'est un peu plus complexe, cela donne une bonne idée du fonctionnement d'internet.