# Internet, suite et fin

>  Nous avons vu ce qu'était [Internet](https://gitlab.com/Poueteuh/snt/-/tree/main/sequences/internet/definition) et aujourd'hui nous sommes capables de faire la différence entre Internet, le web, les protocoles...Mais une chose que l'on n'a pas vu, c'est comment nous en sommes arrivés à l'Internet que l'on connait. 
>
> Celui qui permet aux humains de communiquer. Celui qui offre la possibilité à des millions de personnes de devenir des influenceuses et influenceurs. Celui qui ouvre la voie à un monde hyper connecté via [L'IOT](https://fr.wikipedia.org/wiki/Internet_des_objets) .

Dans cette activité, vous devrez, à l'aide d'une [frise chronologique](https://cdn.knightlab.com/libs/timeline3/latest/embed/index.html?source=1A-dh_QtvHgeoii2mbuvJbQArq_ESMhUyXFqyVNqgI68&font=Default&lang=en&initial_zoom=2&height=650), réflechir et analyser plusieurs questionnements.

Il vous faudra regarder les vidéos, aller lire les pages wikipedia en cliquant sur les liens, afin de répondre aux différentes questions.

**Les questions sont à recopier sur le format de votre choix : docx, libreOffice, papier manuscrit scanné...et à envoyer via Pronote**

Utilisez vos propres termes et expressions, le but n'est pas de fournir une copie parfaite, mais que vous montriez que vous avez compris comment nous en sommes, aujourd'hui, arrivé à l'Internet que l'on connait, en partant parfois, de très loin.

-------------

## Première partie

- En regardant la frise, on voit que les technologies de communication ne datent pas d'hier. Connaissez vous une méthode de transmission de message datant d'avant le 18eme siècle ?
- En reprenant l'alphabet Morse, écrivez votre prénom avec les symboles correspondants.
- La première transmission télégraphique de l'histoire fut effectuée sur une distance de soixante (60) kilomètres. À votre avis, quelle invention a t-on attendu pour pouvoir créer ce genre de technologie ?
- Dès 1858, un câble est tiré entre l'Europe et les États-Unis afin de faciliter les communications. Aujourd'hui, il en existe beaucoup plus ! En vous aidant de [ce lien](https://www.submarinecablemap.com), que pouvez vous dire de la position de la France ?
- En 1934, [Paul Otlet](https://fr.wikipedia.org/wiki/Paul_Otlet) imagine un système de lecture de livres et de ressources via une interface d'écran et de numéro de téléphone à composer. Aujourd'hui, quels services peut-on rapprocher de cette idée ?
- Lors de l'invention du premier modem, de combien était la vitesse de transmission des données ? Aujourd'hui, on estime que la vitesse moyenne des connexions françaises est de 30 Mbits/sec. Combien de fois est ce plus rapide ? Rappel : 1 octet = 8 bits.

---------

## Seconde partie

- En regardant les exemples de premiers ordinateurs tels que l'ENIAC, quelle est la principal différence *physique* avec nos machines d'aujourd'hui ?
- En France, nous avions une technologie concurrente à Internet...Savez vous comment s'appelait l'appareil crée par France Télécom ? [La réponse se trouve dans cette vidéo](https://www.ina.fr/ina-eclaire-actu/video/ryc9806084890/rennes-presentation-du-systeme-minitel-annuaire-electronique)
- Combien de mails envoie t-on chaque jour dans le monde ? À votre avis, cela a t-il un impact sur l'écologie ? Pourquoi ?
- Pourquoi a t-on inventé le protocole [IPV6](https://www.arcep.fr/la-regulation/grands-dossiers-internet-et-numerique/lipv6.html) ?
- À votre avis, quelle sera la prochaine grande évolution dans le monde des télécommunications ? Utilisez votre imagination !



