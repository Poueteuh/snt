# Moteur de recherche

## 1. Contexte

### 1.1. Définition

Une recherche sur le web se fait à l'aide d'un __moteur de recherche__.

> Un __moteur de recherche__ est une application web permettant à un utilisateur d'effectuer une recherche en ligne (ou recherche internet), c'est-à-dire de trouver des ressources à partir d'une requête composée de termes.

Il ne faut pas confondre le navigateur et le moteur de recherche.

Un navigateur permet d'afficher du contenu web après avoir effectué une requête auprès d'un serveur, le navigateur utilise le moteur de recherche pour effectuer des recherches.

### 1.2. Historique

Ci-dessous, une chronologie des moteurs de recherche de 1990 à 2014.

<img src="https://www.mauricelargeron.com/wp-content/uploads/2014/09/evolution-des-moteurs.jpg" width="50%" />



Source : www.mauricelargeron.com

### 1.3. Problématique

__Comment un moteur de recherche génère les résultats de recherche ?__

## 2. Principe de fonctionnement

### 2.1. À Faire

✏ Répondre aux questions suivantes en vous aidant de la vidéo :

https://www.youtube.com/watch?v=pMywV9ZLS4M

1. __Sur quel programme repose un moteur de recherche ?__ 
2. __Qu'effectue ce programme ?__ 
3. __Comment un moteur de recherche répond à une recherche effectuée par un utilisateur ?__ 
4. __Comment sont classés les résultats de la recherche ?__ 

### 2.2. Bilan

Sur le principe, ils fonctionnent généralement en 3 processus :

1. L'__exploration__ ou _crawl_ : le web est systématiquement exploré par un __robot d'indexation__ suivant récursivement tous les hyperliens qu'il trouve et récupérant les ressources jugées intéressantes.
2. L'__indexation__ des ressources récupérées consiste à extraire les mots considérés comme significatifs du corps des pages à explorer. Les urls des pages et les mots clés associés sont enregistrés dans une base de données organisée comme un gigantesque dictionnaire.
4. La __recherche__ correspond à la partie requêtes du moteur, qui restitue les résultats. Un algorithme est appliqué pour identifier dans le corpus documentaire (en utilisant l'index), les pages qui correspondent le mieux aux mots contenus dans la requête, afin de présenter les résultats des recherches par ordre de pertinence supposée.

## 3. Rang d'une page web

### 3.1. Principe

> Le __rang d'une page__ est sa place, son ordre dans la liste des résultats d'une recherche.

En 1998, deux jeunes doctorants de l'université de Stanford, __Larry Page__ et __Sergey Brin__ publiaient un article intitulé "_The PageRank Citation Ranking: Bringing Order to the Web_" présentant les résultats d'un nouvel algorithme permettant de __classer les pages web__ selon leur popularité et montrant la précision de cet algorithme sur un nouveau moteur de recherche appelé ... Google !

Le principe revient à modéliser le web comme un graphe dont les pages sont les nœuds (ronds) et les hyperliens les arrêtes (flêches). 

À chaque page est associé un nombre positif entre 0 et 1, appelé __score de la page__ (PageRank en anglais).

![](../assets/graph_0.png)

La méthode, appelée __marche aléatoire__, peut être appliquée :

```txt
1. Choisir une page de départ,
2. Marquer la page comme visitée une fois,
3. Déterminer au hasard (lancé de dé, ...) une page à suivre,
4. Se déplacer à la nouvelle page,.
5. Recommencer la procédure à l’étape 2.
```

Notre utilisateur va se balader de pages en pages, en faisant son petit tour du net. A chaque fois que l'utilisateur tombe sur une page donnée, cette page gagne un point. 

Et à la fin, la page ayant le plus de point est alors la page la plus populaire du réseau !

### 3.2. À Faire

Le robot d’un moteur de recherche (spider, crawler) a permis d’établir les relations suivantes entre quatre pages web. Elles sont modélisées dans le graphe ci-dessous.

![](../assets/graph_1.png)

Dans ce graphe, la flèche allant de 1 vers 2 signifie que la page 1 référence la page 2 et l’absence de flèche de 2 vers 4 signifie que la page 2 ne référence pas la page 4.

1. ✏ Appliquer la méthode du marcheur aléatoire en complétant le tableau ci-dessous. On travaillera en binôme et on effectuera 20 visites.

| | Page 1 | Page 2 | Page 3 | Page 4 |
| :--: | :--: | :--: | :--: | :--: |
| __Nombre de visites (sur un total de 20)__ | | | | |
| **PageRank** | | | | |

2. ✏ Comparer vos résultats avec ceux des autres binômes de la classe. Que peut-on en conclure ?
3. ✏ Rassembler les résultats de toute la classe et compléter le nouveau tableau ci-dessous :

| | Page 1 | Page 2 | Page 3 | Page 4 |
| :--: | :--: | :--: | :--: | :--: |
| __Nombre de visites (sur un total de 20)__ | | | | |
| **PageRank** | | | | |

4. ✏ Reporter le PageRank en % sur le graphe

![](../assets/graph_1_percent.png)

## 4. Analyse des résultats de recherche

### 4.1. À Faire 

1. ✏ Effectuer des recherches (c-a-d en saisissant les mêmes termes) en utilisant des moteurs de recherche tel que :

- [Google](https://google.fr)
- [Qwant](https://qwant.fr)
- [Bing](https://bing.fr)
- [Lilo](https://search.lilo.org/)

2. ✏ Établir si possible les différences observées...

### 4.2. Explication

Les différences observées peuvent s'expliquer sur le __modèle économique__ des moteurs de recherches.

Les sites dont la recherche est le principal service se financent par la vente de __publicité__.

Selon Wikipédia, le __financement par la publicité__ consiste à présenter des publicités correspondant aux mots recherchés par le visiteur. 

L'annonceur achète des mots-clés : par exemple une agence de voyage peut acheter des mots-clés comme « vacances », « hôtel » et « plage » ou « Cannes », « Antibes » et « Nice » si elle est spécialisée dans cette région. 

Cet achat permet d'obtenir un référencement dit « référencement payant » à distinguer du référencement dit « référencement naturel ».

Le moteur de recherche peut afficher la publicité de deux manières : en encart séparé ou en l'intégrant aux résultats de la recherche.

### 4.3 À Faire

Répondre aux questions suivantes après lecture de [l'article](https://www.slate.fr/story/100441/brin-page-google-pub)

1. ✏ Selon l'article, quels sont les impacts / risques du modèle de financement par la publicité sur les résultats de recherche ?
2. ✏ À quel principe, déjà vu, le modèle par financement peut-il s'opposer ? 

## 5. Synthèse

Recopier et compléter le texte à trou suivant :

Un moteur de recherche est un logiciel qui ...........(2) en fonction de ...........(2).

Pour cela :

1. il utilise des ...........(3) qui ...........(4) les pages du web et génère un ...........(5).
2. Le moteur renvoie les résultats sous forme de ...........(6) où chaque page a un ...........(7).

Les sociétés qui développent des moteurs de recherche se financent par ...........(8). 

