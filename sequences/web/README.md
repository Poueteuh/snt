# Découverte du Web



## ***Activité n°1 :  Le navigateur***

Le "World Wide Web", plus communément appelé "Web" a été développé au CERN (Conseil Européen pour la Recherche Nucléaire) par le Britannique *Sir Timothy John Berners-Lee* et le Belge *Robert Cailliau* au début des années 90. 

À cette époque les principaux centres de recherche mondiaux étaient déjà connectés les uns aux autres, mais pour faciliter les échanges d'information Tim Berners-Lee met au point le système hypertexte. 

Le système hypertexte permet, à partir d'un document, de consulter d'autres documents en cliquant sur des mots clés. 

Ces mots "cliquables" sont appelés hyperliens et sont souvent soulignés et en bleu. Ces hyperliens sont plutôt connus aujourd'hui sous le simple terme de "liens".

![premier_site](assets/premier_site.png)

Cette première page web est toujours consultable [ici]( http://info.cern.ch/hypertext/WWW/TheProject.html).

Tim Berners-Lee développe le premier navigateur web (logiciel permettant de lire des pages contenant des hypertextes), il l'appelle simplement "***WorldWideWeb***". Il faudra attendre 1993 et l'arrivée du navigateur web "NCSA Mosaic" pour que le web commence à devenir populaire en dehors du petit monde de la recherche.

***Exercice*** 

Donnez les noms des navigateurs auquels appartiennent les logos ci dessous :



| Logo                                                   | Nom  |
| ------------------------------------------------------ | ---- |
| ![chrome.svg](assets/chrome.svg)                       |      |
| ![chromium.png](assets/chromium.svg)                   |      |
| ![edge.svg](assets/edge.svg)                           |      |
| ![firefox.svg](assets/firefox.svg)                     |      |
| ![safari.svg](assets/safari.svg)                       |      |
| ![opera.svg](assets/opera.svg)                         |      |
| ![internet_explorer.svg](assets/internet_explorer.svg) |      |

------

## ***Activité n°2 : Surfer sur le Web***

Techniquement le web se base sur trois choses : le protocole *HTTP* (HyperText Transfert Protocol), les *URL* (Uniform Resource Locator) et le langage de description *HTML* (HyperText Markup Language). Nous aurons, très prochainement l'occasion de revenir sur ces trois éléments.

Une chose très importante à bien avoir à l'esprit : beaucoup de personnes confondent "web" et "internet". Même si le "web" "s'appuie" sur internet, les deux choses n'ont rien à voir puisqu'"internet" est un "réseau de réseaux" s'appuyant sur le protocole IP (voir le module Internet) alors que, comme nous venons de le voir, le web est la combinaison de trois technologies : HTTP, URL et HTML. D'ailleurs on trouve autre chose que le "web" sur internet, par exemple, les emails avec le protocole SMTP (Simple Mail Transfert Protocol) et les transferts de fichiers avec le protocole FTP (File Transfert Protocol).

## Comment ?

Les navigateurs interprètent des **codes** (HTML et CSS) afin d’afficher des pages Web. Il s’agit de langages de description (et non pas de programmation) qui permettent de créer des sites web.
Les langages **HTML** et **CSS** sont à la base du fonctionnement de tous les sites Web.

Le langage HTML (HyperText Markup Language, langage de balisage hypertexte) a été inventé par Tim Bernes-Lee en 1991.

Son rôle est de gérer et d’organiser le contenu de la page web (titres, textes, images, etc.). Il répond à une norme très précise.

Le langage CSS (Cascade Style Sheets, feuilles de style en cascade) est venu compléter le langage HTML en 1996. Son rôle est de gérer l’apparence de la page web (agencement, positionnement, décoration, couleurs, taille du texte, etc.).

Autrement dit, le contenu est écrit dans le fichier HTML et la mise en forme est écrite dans le fichier CSS.

Vous pouvez examiner le code html du site du lycée :

https://charlotteperriand.etab.ac-lille.fr -> clic droit sur la page -> afficher code source 

![afficher_source](assets/afficher_source.png)



Vous aurez donc ce genre de page :

![source](assets/source.png)

Alors que votre navigateur vous affiche ceci :

![site_lycee](assets/site_lycee.png)

Est ce de la magie ? Du vaudou ? Que nenni !

***Exercice***

Nous allons utiliser le logiciel *Notepad ++* afin d'ouvrir le ficher index.html

Repérez les différents éléments qui forment la structure de base d’une page HTML

·   Le doctype < !DOCTYPE html > : il s’agit de la toute première ligne du document. C’est elle qui indique qu’il s’agit bien d’une page web HTML et qu’elle est écrite en HTML5.

·   La balise < html > : il s’agit de la balise principale du code. Elle définit un conteneur qui englobe tout le contenu de la page web. Cette balise est fermée à l’aide de la balise </ html> située à la dernière ligne du code.

La balise < head > : il s’agit d’une balise définissant le conteneur d’en-tête de la page. Ce conteneur donne des informations générales sur la page comme son titre (donné par la balise < title >), son encodage (donné par la balise < meta charset=... /> et utile pour la gestion de certains caractères), etc. 

·   Celles-ci ne sont pas affichées sur la page, mais sont extrêmement importantes puisqu’elles sont nécessaires au navigateur pour une bonne interprétation du code.

·   La balise < body > : il s’agit d’une balise définissant le conteneur du corps de la page. Ce conteneur contient tous les éléments qui seront affichés sur la page.

***Le conteneur d’en-tête***

·   Indiquez les lignes correspondantes au conteneur d’en-tête du fichier.

·   Précisez le titre de la page. Indiquez où il apparaît lorsque la page est exécutée par le navigateur. Modifiez ce titre.

·   Indiquez l’encodage du fichier. Expliquer ce qui se passe sur la page web si vous supprimez la ligne indiquant l’encodage.

***Le corps de la page web***

·   Indiquez les lignes correspondantes au corps de la page web.

·   Modifiez le titre < h1 > Web en < h2 > Web. Indiquez ce que vous observez sur la page web.

·   Dites comment mettre des mots en gras. Mettez en gras les mots HTML et CSS.

·   Précisez quelle est la balise qui permet d’insérer une image.

·   Indiquez à quoi peut servir la balise < span >.

***Liaison CSS***

·   Repérez quelle est la ligne de code qui permet de lier le fichier index.html à sa feuille de style.

·   Indiquez ce qui se passe sur la page web si on supprime cette ligne.

***Fichier CSS***

·   Ouvrez le fichier style.css à l’aide de Notepad++. Précisez comment il est structuré.

·   Modifiez le style CSS pour que les balises < h1 >  soient écrites en bleu.

·    Indiquez à quoi correspondent les attributs width et height de la balise img. Expliquez comment les modifier pour que l’image soit un rectangle de taille 200 x100 pixels.

Les paragraphes sont créés avec les balises < p > .

·    Essayez de modifier la couleur des paragraphes à l'aide du fichier CSS.

·    Indiquez comment modifier les fichiers index.html et style.css pour que les mots HTML et CSS soient écrits en bleu et en italique, mais sans modifier le reste du document.