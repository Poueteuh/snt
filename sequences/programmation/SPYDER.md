# Prise en main de l'outil Spyder

Le logiciel *__Spyder__* (Scientific Python Development EnviRonment) est un outil qui nous permettra, au fur et à mesure de l'année, de travailler sur la programmation en langage python. <br>

Il s'agit, pour simplifier, de l'interface entre la machine et vous : nous emploierons un langage de programmation afin de transmettre à l'ordinateur des instructions qui lui seront traduites en langage machine.

<br>

Ainsi, aucun risque de ne pas se faire comprendre par l'ordinateur.

```mermaid
flowchart LR
  Nous -- Langage Python --> Spyder -- Langage Machine --> Ordinateur
```

-----

Commençons donc par voir à quoi ressemble l'interface de *__Spyder__* : 
<br>

![interace_base.png](assets/interface_base.png)

>   1.  *La fenêtre principale*, à gauche, va vous permettre de taper du *code* et donc de *__programmer__*.<br>
  2. En haut à droite, *l'explorateur* vous donnera des conseils, vous affichera les fichiers ou les *__variables déclarées__* dans le programme. 
  3. En bas à droite, *__l'interpreteur__* vous indiquera le résultat du code executé : on parlera également de *__sortie__*

En pratique, cela donne cet aspect :

> ![Test_code.png](assets/test_code.png)

Ici, on peut voir le resultat du code que j'ai écrit.

----

Mais avant de se mettre au travail, il est essentiel de vous *__approprier__* le logiciel ! Pour celà, on va tout d'abord passer Spyder en français :

<li>Cliquez sur l'icône <b>outils</b> </li>

![Icone_outils.png](assets/icone_outils.png)

    


<li> Puis dans le menu qui s'affiche, selectionner <b><i><u>Application</u> </i> </b> puis à droite, l'onglet <b>Advanced Settings </b> </li>
<br>

Et enfin choisissez Français, afin de pouvoir travailler dans de bonnes conditions.
    
![Changer_langue.png](assets/changer_langue.png)    

----

> Il reste une petite chose à faire si vous le souhaitez : basculer sur le thème sombre.
Ce choix est personnel, selon vos préférences, choisissez donc bien : vous allez passer l'année à travailler avec cet outil, autant avoir une interface qui vous plaise !

Dans le même menu, selectionnez Apparance, tout en haut. Puis changez (ou non !) le *__thème de l'interface__* et / ou le *__thème des icônes__*

![Apparence.png](assets/apparence.png)

-----

Puisque l'on parle des icônes, celles ci sont situées ici, au dessus de la fenêtre de code :

![Raccourcis.png](assets/raccourcis.png)

Retenez juste son emplacement pour le moment.

Maintenant, pour en finir avec le paramètrage de Spyder, il vous reste une tâche : décider de l'emplacement de vos fichiers et travaux. <br>

Il va falloir selectionner un emplacement accessible (votre espace perso, un dossier de votre session sur le disque dur, ou votre clé usb) auquel vous êtes certains et certaines d'avoir accès.

Pour cela, regarder en haut à droite de la fenêtre de Spyder, juste à côté de l'icone *outils* :

![Dossier_source.png](assets/dossier_source.png)

Cliquez donc sur l'icone *Dossier* qui se trouve à droite de la barre d'adresse afin de selectionner le répertoire source de vos travaux.

----

Reste une dernière chose à voir (promis c'est bientôt la fin) !

En Python, tout ce que vous pourrez écrire comme code se fait sur des fichiers .py (l'extension de fichier Python)

Vous pouvez trouvez cette information juste au dessus de la fenêtre principale de votre code :

![fichier_py.png](assets/fichier_py.png)

Ici on nous rappelle donc le chemin du fichier, ainsi que son nom. Soyez attentifs lorsque vous débuterez un nouveau TP, de vous assurez que le fichier sur lequel vous travaillez soit bien le bon !

----

Et maintenant, au boulot avec le sourire 
