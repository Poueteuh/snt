# TD - Premier pas en Python

## 1. La console

La __console__ est un endroit où l'on peut écrire du code en Python.

Le code sera exécuté immédiatement dès validation de la ligne, c'est-à-dire dès que vous appuyer sur le bouton Run.

### 1.1. À Faire

1. Éxécuter le code suivant (_sans les >>>_) :

```python
>>> 5 + 2
```


```python

```

2. Quel est le résultat obtenu ?

#### Bilan 1.1

- A travers ce premier exemple, on constate que Python sait faire des calculs !!
- Lorsque l'on appuie sur le bouton Run, python lit le code 5 + 2, calcule l'opération associée et restitue le résultat.

### 1.2. À Faire

1. Écrire et exécuter successivement les instructions suivantes (_sans les >>> et ???_) `

```python
>>> 5 - 2
???
>>> 5 * 2
???
>>> 5 / 2
???
>>> 5 // 2
???
>>> 5 % 2
???
>>> 5 ** 2
???
```

2. Quels sont les résultats obtenus ? Compléter le tableau suivant en décrivant ce que fait l'opérateur de l'instruction.

| Opérateur | Description |
| :--: | :-- |
| - | | 
| * | |
| / | |
| // | |
| % | |
| * | |

#### Bilan 1.2

- Python permet l'utilisation des opérateurs arithmétiques usuelles $+, -, *, /$ sur les entiers,
- Python permet l'utilisation d'opérateurs arithmétiques avec une syntaxe spécifique comme $//, \%, **$ 

### 1.3. À Faire

Exécuter le code suivant. Que constatez-vous ?

```python
>>> # 3 + 2
???
```


```python

```

### 1.4. À Faire

1. En prenant votre âge à votre prochaine date d'anniversaire (en nombre d'années), écrire une instruction qui calcule le nombre de jours équivalents (on considère qu'une année correspond à 365 jours)


```python

```

2. Sachant qu'à mon prochain anniversaire j'aurai 9490 jours, écrire une instruction qui calcule mon âge, en nombre d'années :


```python

```

3. Sachant que mon logement se situe au 256 de la rue (dont je tairais le nom pour ne pas être localisé ;-)) et que toutes les maisons font 12 mètres et demi de longueur, à quelle distance se situe ma maison par rapport au début de la rue ?


```python

```

#### Bilan 1.4

- Outre des entiers, Python permet d'utiliser des nombres décimaux en utilisant le symbole "." pour séparer la partie entière et la partie décimale.

## 2. Les variables

En informatique, il est indispensable de conserver des informations de natures diverses. Par exemple, votre smartphone enregistre dans sa mémoire votre numéro de téléphone, les numéros de téléphones de vos contacts, vos messages (SMS), vos photos, vos applications, etc. Chacune de ces informations est stockée à un endroit précis dans la mémoire dans ce qu’on appelle une variable.

### 2.1. Définition

> Une __variable__ est un __espace de stockage__ de la mémoire (une case mémoire). Chaque variable est caractérisée par son __nom__, son __type__ et sa __valeur__.

## 3. Valeur des variables

### 3.1. Affecter une valeur à une variable

#### 3.1.1. À Faire

Copier et exécuter les instructions suivantes

```python
>>> a = 5
>>> b = 2
>>> a + b
???
```


```python

```

#### Bilan 3.1.1

1. La syntaxe pour déclarer et affecter une valeur a une variable est `nom de la variable = valeur`
2. Pour les instructions 1 et 2, Le signe `=` indique que la valeur 5 est affectée à la variable `a` et 2 à la variable `b`.
3. Lorsque Python interprète l'instruction 3, il lit les valeurs associées aux variables et effectue l'opération.

## 4. Type des variables

Dans les travaux à faire précédent, nous avons manipulé essentiellement des entiers.

Python gère différents types de variables. 

L'objet de cette section est d'expliciter la notion de type et les opérations associées :

- le type __entier__ : il désigne les entiers relatifs (positifs ou négatifs). En Python on parle du type `int` (pour integer qui signifie « entier » en anglais) ;
- le type __flottant__ : il désigne les nombres décimaux (à virgule). En Python on parle du type `float` (pour floating qui signifie « flottant » en anglais)
- le type __chaîne de caractères__ : il désigne toute suite ordonnée de caractères. En Python on parle du type `str` (pour string qui signifie « chaîne » en anglais).

### 4.1. À Faire

Indiquez le type des variables permettant de stocker (sur votre smartphone) les informations suivantes :

| Variable | Type identifié |
| :--: | :-- |
| le nom d’un contact | |
| le numéro de téléphone d’un contact | |
| un SMS | |
| l’heure du réveil | |
| le code de votre partage de connexion Wi-Fi | |
| le pourcentage affiché de batterie restante | |
| les notes aux deux derniers devoirs de Mathématiques de l'année dernière | |

## 4.2. Type entier (int)

Pour affecter (on dit aussi assigner) la valeur 17 à la variable nommée `age`

```python
>>> age = 17
```

La fonction `print` affiche la valeur de la variable :

```python
>>> print(age)
17
```

La fonction `type` renvoie le type de la variable :

```python
>>> type (age)
<type 'int'>
```


## 4.3. Type flottant (float)

Pour affecter (on dit aussi assigner) la valeur 1,70 à la variable nommée `taille`

```python
>>> taille = 1.7
```

La fonction `print` affiche la valeur de la variable :

```python
>>> print(taille)
1.7
```

La fonction `type` renvoie le type de la variable :

```python
>>> type (taille)
<type 'float'>
```
La notation scientifique est acceptée :
```python
>>> taille = 1.7e2 #taille en cm où 1m = 100cm = 10**2cm
>>> print (taille)
170.0
```

## 4.4. Type chaine de caractères (str)

Pour affecter (on dit aussi assigner) la valeur `lundi` à la variable nommée `jour`

```python
>>> jour = 'lundi' # autre possibilité jour = "lundi"
```

La valeur doit être entre guillemet simple (autrement dit côte) ou guillemet double.

La fonction `print` affiche la valeur de la variable :

```python
>>> print(jour)
lundi
```

```python
>>> mois = "septembre"
>>> print (jour, mois) # print permet d'afficher plusieurs valeurs à la suite, ne pas oublier la virgule
lundi septembre
```

La fonction `type` renvoie le type de la variable :

```python
>>> type (jour)
<type 'str'>
```

La fonction `len` renvoie la longueur (length) de la chaîne de caractères :

```python
>>> print(len(jour))
5
```

### 4.4.1 À Faire

Copier et exécuter les instructions suivantes

```python
>>> a = "Je suis"
>>> b = " Yoda "
>>> a + b 
???
>>> b + a
???
```


```python

```

### Bilan 4.4.1

Comme vous le voyez, on peut ajouter deux chaînes : c'est une ___concaténation___, la mise bout à bout de plusieurs chaînes de caractères.

### 4.4.2 À Faire

Copier et exécuter les instructions suivantes

```python
>>> mot = "lycée"
>>> print(mot[0])
???
>>> print(mot[1])
???
>>> print(mot[1:4])
???
>>> print(mot[2:])
???
>>> print(mot[:2])
???
>>> print(mot[-1])
???
```

### Bilan 4.4.2

Selon les résultats obtenus, que permet d'obtenir l'utilisation des crochets sur une variable de type chaine de caractères :

- variable[indice] : ???
- variable[indice1:indice2] : ???
- variable[indice:] : ???
- variable[:indice] : ???
- variable[-1] : ???

Par convention, on appelle _indice_ les valeurs entre crochets.

## 4.X Conversion de type

Cependant, on ne peut pas ajouter n'importe quoi.

Copier et exécuter les instructions suivantes :

```python
>>> a = 'Je mesure '
>>> b = 1.12
>>> c = 'm'
>>> a + b + c
???
```


```python

```

Cela ne marche pas car les variables ne sont pas du même *type*. Il faut les convertir en utilisant :

- soit la fonction `str(nombre)` qui convertit le nombre en chaîne.
- soit la fonction `int(chaîne)` qui convertit, si possible, la chaîne en entier.
- soit la fonction `float(chaîne)` qui convertit, si possible ,la chaîne en flottant.

```python
>>> str(b)
???
>>> a + str(b) + c
???
>>> str(b) + c + a
???
```


```python

```

## 5. Nom des variables

Chaque variable possède un nom qui permet d’identifier l’emplacement mémoire correspondant.

Dans le langage Python, il y a des règles à respecter pour nommmer les variables. Voici celles qui vous concernent :

- __Règle 1__ : un nom ne peut contenir que des lettres (a-z, A-Z), des chiffres (0 - 9) et le caractère _ (underscore).
- __Règle 2__ : un nom ne peut pas commencer par un chiffre.
- __Règle 3__ : les noms sont sensibles à la casse, cela signifie qu’il y a une distinction entre les minuscules et les majuscules : la variable nommée `nsi` est différente de la variable `Nsi`.
- __Règle 4__ : il est préférable de toujours choisir un nom de variable représentatif : par exemple, si vous voulez stocker le nom d’une personne dans une variable, il est préférable de l’appeler nom plutôt que x.
- __Règle 5__ : il est préférable de ne pas utiliser de caractères accentués dans le nom d’une variable (nous n’entrerons pas dans le pourquoi du comment).

### 5.1. À Faire

1. Quels sont les noms de variables incorrects parmi ceux proposés ? Vous indiquerez pourquoi.

| Noms de variable | Correct ou Incorrect | Pourquoi (si Incorrect) |
| :--:| :--: | :-- | 
| `prix achat` | | |
| `prix_achat` | | |
| `note` | | | 
| `2ndeG` | | |
| `SecondeG` | | | 
| `Seconde:G`| | |
| `dix-huit` | | |

2. Proposez un nom de variable permettant de stocker :

| Variable | Nom de variable |
| :-- | :--: |
| le nombre de personnes aux yeux bleus de Premiere G | |
| le tarif d’un repas au self | |
| l’aire d’un triangle (il n’y a qu’une seule figure) | |
| la note à un devoir d’anglais | |

## 6. Les fonctions mathématiques

Pour utiliser les fonctions mathématiques, il faut commencer par importer le module math :

```python
>>> import math
```

La fonction `dir` renvoie la liste des fonctions et données d'un module :

```python
>>> dir(math)
['__doc__', '__name__', '__package__', 'acos', 'acosh', 'asin', 'asinh', 'atan',
'atan2', 'atanh', 'ceil', 'copysign', 'cos', 'cosh', 'degrees', 'e', 'erf',
'erfc', 'exp', 'expm1', 'fabs', 'factorial', 'floor', 'fmod', 'frexp', 'fsum',
'gamma', 'hypot', 'isinf', 'isnan', 'ldexp', 'lgamma', 'log', 'log10', 'log1p',
'modf', 'pi', 'pow', 'radians', 'sin', 'sinh', 'sqrt', 'tan', 'tanh', 'trunc']
```

Pour appeler une fonction d'un module, la syntaxe est la suivante : `module.fonction(arguments)`

Exemple :

```python
>>> math.sqrt(9)
3.0
```

La fonction `help` affiche l'aide, c'est-à-dire ce que fait la fonction

```python
>>> help(math.sqrt)
Help on built-in function sqrt in module math:

sqrt(x, /)
    Return the square root of x.
```

## 7. Saisie utilisateur : fonction `input`

Copier et exécuter l'instruction suivante :

```python
>>> input("As-tu un message ? ")
???
```


```python

```

Bilan : La fonction `input` permet d'obtenir une valeur saisie par l'utilisateur.

___N.B : La valeur saisie est toujours du <type 'str'>___

## 6. Exercices

### Exercice 1

L’indice de masse corporelle (IMC) est le seul indice validé par l’Organisation mondiale de la santé pour évaluer les éventuels risques pour la santé.

Il s'exprime par la formule `IMC = poids (kg) / taille (m²)`

Je mesure 170cm pour un poids de 3/4 de quintal. 

1. Exprimer les différentes données sous forme de variables et l'expression permettant de calculer l'IMC.
2. Donner les instructions permettant de donner les types des différentes variables et du résultat obtenu.


```python

```

### Exercice 2

Afficher la taille en octets et en bits d'un fichier de 536 kio.

On donne : 

- 1 kio (1 kibi octet) = 1024 octets !!!
- 1 octet = 8 bits


```python

```

### Exercice 3

On considère le programme Python suivant.

```python
a = 8
b = 3
a = a - 4
b = 2 * b
a = a + b
print(a)
```

1. Combien de variables sont utilisées ? Combien de valeurs différentes ont-elles prises au cours de l’exécution du programme ?
2. Quelle est la valeur finale de la variable `a` ?
3. Il est possible d’afficher plusieurs valeurs avec la fonction `print`. Par exemple, si on veut afficher les valeurs des variables `a` et `b` on écrit simplement `print(a, b)`. Modifiez la dernière ligne du programme et exécutez-le.


```python

```

### Exercice 4

On considère le programme de calcul suivant.

- a prend la valeur 5
- Multiplier a par 3
- Soustraire 4 au résultat
- Elever le résultat au carré
- Afficher le résultat

Écrire un programme Python permettant de coder ce programme de calcul. Vérifiez ensuite en l'exécutant.


```python

```

### Exercice 5

Sur un site de vente en ligne, on peut acheter différents articles. Un paquet de farine d'un kg coûte 1,15 € et une boîte de six oeufs coûte 1,50 €.

Écrire un programme Python qui utilise deux variables pour stocker le nombre de paquets de farine et de boîtes d'oeufs souhaités puis, qui calcule et affiche le prix total à payer pour la commande.


```python

```

### Exercice 6

À partir de deux variables `prenom` et `nom`, afficher les initiales (par exemple LM pour Léa Martin).


```python

```

### Exercice 7

L'identifiant d'accès au réseau du lycée est construit de la manière suivante : initiale du prénom puis les 8 premiers caractères du nom.

Exemple : Alexandre Lecouturier → alecoutur

A partir des deux variables `prenom` et `nom`, construire l'identifiant.


```python

```

### Exercice 8

Écrire un programme qui donne la pointure du pied demandé à l'utilisateur et affiche la taille du pied sachant qu'en France, la pointure correspond à 3/2 de la longueur du pied exprimée en centimètres et à laquelle on a au préalable ajouté un centimètre. (Vous pouvez vérifier votre résultat via ce [site](https://2-grande-taille.com/conversion-pointures)


```python

```

### Exercice 9

![Triangle rectangle, Source : Wikipedia](https://upload.wikimedia.org/wikipedia/commons/8/85/Triangle_aire.png)

Écrire un programme qui demande 2 entiers sous la forme "a, h" où a et h sont des nombres à 1 seule chiffre représentant respectivement la base et la hauteur d'une triangle rectangle. Afficher l'aire du triangle.


```python

```

### Exercice 10

![Triangle quelconque, Source : Wikipedia](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/Triangle_with_notations.svg/320px-Triangle_with_notations.svg.png)

Écrire un programme qui demande 3 entiers sous la forme "a, b, c" où a, b et c sont des nombres à 1 seule chiffre et calculer l'aire du triangle quelconque formés par ces 3 longueurs.

__N.B : Le calcul de l'aire d'un triangle quelconque s'effectue par l'application de la formule du Héron.__


```python

```
