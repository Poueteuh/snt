# Programmation Python

## Présentation

Ce chapitre a pour objectif de répondre aux questions suivantes : 

- Qu'est-ce que __programmer__ ?
- Qu'est que __Python__ ?
- Comment programmer en Python ?

## Progression

```mermaid
flowchart LR;
    A[1. Généralités]-->B[2. Prise en main de Python];
    B-->C[3. Les variables];
    C-->D[4. Les instructions conditionnelles];
    D-->E[5. Les boucles];
    E-->F[6. Les fonctions];
    
    click A "./programmation/GENERALITES.md" "Lien vers 1. Généralités" _self
    click B "./programmation/SPYDER.md" "Lien vers 2. Prise en main de Python" _self
    click C "./programmation/chapitre_1/" "Lien vers 3. Les variables" _self
    click D "./programmation/chapitre_2/" "Lien vers 4. Les instructions conditionnelles" _self
    click E "./programmation/chapitre_3/" "Lien vers 5. Les boucles" _self
```

## Attendus du Programme

| Contenus                                                     | Capacités attendues                                          |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Affectations, variables<br />Séquences<br />Instructions conditionnelles<br />Boucles bornées et non bornées<br />Définitions et appels de fonctions | Écrire et développer des programmes pour répondre à des problèmes et modéliser des phénomènes physiques, économiques et sociaux. |
