# Prise en main de Python

## Introduction

Python est un langage de programmation interprété : on tape le code des programme dans un éditeur de code qui, lorsqu'on lui demande d'exécuter le code, utilise un interpréteur Python installé sur votre machine afin de lire le code et de le traduire à la machine afin qu'elle l'exécute au fur et à mesure.

Il existe de nombreux éditeur de code en Python.

D'autres éditeurs de code existent comme [Spyder](https://www.spyder-ide.org/), [Pyzo](https://pyzo.org/start.html), [Edupython](https://edupython.tuxfamily.org/), [Thonny](https://thonny.org/), d'autres permettent d'être exécuté en ligne [Basthon](https://console.basthon.fr/).

Nous allons utiliser la majorité du temps ce dernier. Ce document vise à le présenter pour faciliter sa prise en main.

## Basthon

### Définition

Basthon est l’acronyme de « Bac À Sable pour pyTHON ». Il ressemble au mot « baston », c’est une allusion à la « lutte » que peut parfois représenter l’apprentissage de la programmation, l’écriture d’un code ou son débogage.

Basthon est utilisé pour s’initier au langage de programmation Python, sans rien avoir à installer. Il suffit de disposer d’un navigateur (Firefox, Chrome ou Chromium) à jour et d’une connexion à Internet.

Il suffit de se rendre à l'url [https://console.basthon.fr/](https://console.basthon.fr/), l'interface suivante apparait :

![Interface de Basthon](./assets/basthon.png)

### Se repérer dans l'interface

![1. La console, 2. L'éditeur](./assets/basthon_repere.png)

- La partie droite (1 sur l'image) est la `console`
- La partie gauche (2 sut l'image) est l'`éditeur`

Ci-dessous, une description des principaux boutons de l'interface :

| Élément | Description|
| :--: | :-- |
| ![](./assets/bouton_raz.png) | Remise à zéro : la console et la sortie graphique sont nettoyées et l’interprète Python est redémarré. |
| ![](./assets/bouton_console.png) | Affiche la console (à la place de la sortie graphique). |
| ![](./assets/bouton_graphique.png) | Affiche la sortie graphique (à la place de la console) pour visualiser un rendu Turtle, Matplotlib, etc. |
| ![](./assets/bouton_mode.png) | Bascule l’affichage en mode lumineux ou sombre. |
| ![](./assets/bouton_executer.png) | Lance l’exécution du contenu de l’éditeur dans la console. |
| ![](./assets/bouton_upload.png) | Ouvre un fichier Python (.py) dans l’éditeur. |
| ![](./assets/bouton_download.png) | Télécharge le contenu de l’éditeur dans un fichier Python (fichier avec l'extension .py) |

### Première utilisation de l'interface

#### 1. À Faire

Dans la console, écrire l'instruction suivante et appuyer sur la touche ENTRÉE

```python
print("Bonjour le monde !")
```

On constate que l'interpréteur Python affiche `Bonjour le monde !`

#### 2. À Faire

1. Dans la console, écrire l'instruction suivante et appuyer sur la touche ENTRÉE

```python
input("Votre prénom : ")
```

2. On constate qu'une fenêtre de saisie apparait. Saisissez votre prénom et appuyez sur `OK`
3. Quel est le résultat obtenu ?

#### 3. À Faire

1. Dans la console, écrire l'instruction suivante et appuyer sur la touche ENTRÉE

```python
help(print)
```

2. Dans la console, écrire l'instruction suivante et appuyer sur la touche ENTRÉE

```python
help(input)
```

On constate que l'instruction `help` affiche l'aide (en anglais) des instructions passées entre parenthèse. C'est une instruction bien utile pour connaitre le fonctionnement des instructions.

#### 4. À Faire

Dans la console, écrire l'instruction suivante et appuyer sur la touche ENTRÉE

```python
clear
```

On constate que cette instruction vide la console, ce qui est utile pour faire un peu le ménage lorsque l'on a saisi trop de code :-)

## Pour aller plus loin

La documentation complète et officielle de Basthon se trouve à cette url [https://basthon.fr/doc.html](https://basthon.fr/doc.html)

Vous êtes parés pour entamer la [prochaine activité](chapitre_1/README.md) !

